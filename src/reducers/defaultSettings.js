const defaultSettings = {
	SettingsVersion: '1.1',

	ShowComputerInfo: true,
	ShowPowerInfo: true,
	ShowBookmarkBar: true,
	ShowOtherBookmarks: true,
	ShowMobileBookmarks: true,
	UseAltBookmarkFolders: false,
	ShowTopSites: true,
	ShowDownloads: true,
	Use24HourFormat: true,
	ShowSeconds: true,
	CenterClock: true,
	UseAmericanDateFormat: false,
	UseDarkTheme: false
};

export default defaultSettings;
