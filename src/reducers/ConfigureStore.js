import { createStore, compose }        from 'redux';
import { autoRehydrate }               from 'redux-persist';
import reducer                         from './Reducers';

const finalCreateStore = compose(autoRehydrate())(createStore);
const store = finalCreateStore(reducer);

export default store;
