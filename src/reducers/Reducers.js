import { combineReducers } from 'redux';

import settings from './settingsReducer.js';

export default combineReducers({
	settings
});
