import defaultSettings from './defaultSettings.js';

export default function reducer(
	state = {},
	action
){
	switch(action.type){
		case 'LOAD_SETTINGS': {
			let settings = {};
			if(action.payload['reduxPersist:settings']){
				settings = action.payload['reduxPersist:settings'];
			}else if(action.payload['settings']){
				settings = action.payload['settings'];
			}else{
				settings = defaultSettings;
			}
			console.log(settings);
			return settings;
		}
		case 'TOGGLE_SETTING': {
			let toggledSetting = state[ action.payload ];

			if(toggledSetting){
				toggledSetting = false;
			}
			else{
				toggledSetting = true;
			}
			return {
				...state,
				[ action.payload ]: toggledSetting
			};
		}
		case 'RESET_SETTINGS': {
			return defaultSettings;
		}
		case 'CLEAR_SETTINGS': {
			return {};
		}
		case 'VERIFY_SETTINGS': {
			console.log('Checking settings...');
			switch(state.SettingsVersion){
				case undefined :{
					console.log('Settings are from v0.9, resetting...');
					return defaultSettings;
				}
				case '1.0' :{
					console.log('Settings are from v1.0, migrating...');
					return {
						SettingsVersion: '1.1',

						ShowComputerInfo: state.ShowComputerInfo,
						ShowPowerInfo: state.ShowPowerInfo,
						ShowBookmarkBar: state.ShowBookmarkBar,
						ShowOtherBookmarks: state.ShowOtherBookmarks,
						ShowMobileBookmarks: state.ShowMobileBookmarks,
						UseAltBookmarkFolders: defaultSettings.UseAltBookmarkFolders,
						ShowTopSites: state.ShowTopSites,
						ShowDownloads: state.ShowDownloads,
						Use24HourFormat: state.Use24HourFormat,
						ShowSeconds: state.ShowSeconds,
						CenterClock: state.CenterClock,
						UseAmericanDateFormat: state.UseAmericanDateFormat,
						UseDarkTheme: state.UseDarkTheme
					};
				}
				case '1.1' :{
					console.log('Settings are up to date');
					return {...state};
				}
				default :{
					console.log('Settings are invalid, resetting...');
					return defaultSettings;
				}
			}

		}
		default: {
			return state;
		}
	}
}
