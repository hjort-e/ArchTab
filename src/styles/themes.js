import getMuiTheme                  from 'material-ui/styles/getMuiTheme';
import {
	green500,
	grey200,
	blue500,
	lightBlue700,
	grey800,
	redA100,
	redA200,
	redA400,
	fullWhite,
} from 'material-ui/styles/colors';

import {fade} from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';

const themes = {};
themes.light = getMuiTheme({
	palette: {
		primary1Color: green500,
		primary3Color: grey200,
		accent1Color: blue500
	}
});

themes.dark = getMuiTheme({
	spacing: spacing,
	fontFamily: 'Roboto, sans-serif',
	palette: {
		primary1Color: lightBlue700,
		primary2Color: lightBlue700,
		primary3Color: grey800,
		accent1Color: redA200,
		accent2Color: redA400,
		accent3Color: redA100,
		textColor: fullWhite,
		secondaryTextColor: fade(fullWhite, 0.7),
		alternateTextColor: '#303030',
		canvasColor: '#303030',
		borderColor: fade(fullWhite, 0.3),
		disabledColor: fade(fullWhite, 0.3),
		pickerHeaderColor: fade(fullWhite, 0.12),
		clockCircleColor: fade(fullWhite, 0.12),
	},
});

export default themes;
