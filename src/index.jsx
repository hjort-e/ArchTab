import React                                      from 'react';
import { render }                                 from 'react-dom';
import Router                                     from 'react-router/lib/Router';
import Route                                      from 'react-router/lib/Route';
import IndexRoute                                 from 'react-router/lib/IndexRoute';
import hashHistory                                from 'react-router/lib/hashHistory';
import { Provider }                               from 'react-redux';
import { persistStore }                           from 'redux-persist';
import { configureSync, sync }                    from 'browser-redux-sync';
import injectTapEventPlugin                       from 'react-tap-event-plugin';
import store                                      from './reducers/ConfigureStore';

injectTapEventPlugin();

require('./styles/main.sass');
import Dashboard from './components/dashboard/Dashboard.jsx';
import Settings from './components/settings/Settings.jsx';

class App extends React.Component {
	componentWillMount(){
		chrome.storage.local.get(null, settings => {
			store.dispatch({ type: 'LOAD_SETTINGS', payload: settings });
		});
		store.subscribe(() => {this.forceUpdate();});
	}
	componentDidMount(){
		const persistor = persistStore(store, configureSync());
		sync(persistor);

	}
	render () {
		if(Object.keys(store.getState().settings).length === 0){
			return null;
		}else{
			return (
				<Provider store={store}>
					<Router history={hashHistory}>
						<Route path="/">
							<IndexRoute component={Dashboard}/>
							<Route path="settings" component={Settings}/>
						</Route>
						<Route path="*" component={Dashboard}/>
					</Router>
				</Provider>
			);
		}
	}
}

render(<App/>, document.getElementById('app'));
