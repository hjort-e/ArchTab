import React              from 'react';
import { Card, CardText } from 'material-ui/Card';
import FontIcon           from 'material-ui/FontIcon';

class PowerCard extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			battery: false,
			batteryInterval: null
		};
		this.updateBattery();
	}

	getChargeTime(){
		if( this.state.battery.charging ){ // Charging laptop
			let secs = this.state.battery.chargingTime;
			let hours   = Math.floor(secs / 3600);
			let minutes = Math.floor((secs - (hours * 3600)) / 60);

			if (minutes < 10) {minutes = '0'+minutes;}
			return hours+'h '+minutes+'m';
		}
		else if( !this.state.battery.charging ){ // Discharging laptop
			let secs = this.state.battery.dischargingTime;
			let hours   = Math.floor(secs / 3600);
			let minutes = Math.floor((secs - (hours * 3600)) / 60);

			if (minutes < 10) {minutes = '0'+minutes;}
			return hours+'h '+minutes+'m';
		}
	}

	updateBattery() {
		navigator.getBattery().then(
			function(battery) {
				this.setState({
					battery: battery
				});
			}.bind(this)
		);
	}

	componentWillMount(){
		let batteryInterval = setInterval(function() {
			this.updateBattery();
			window.battery = this.state.battery;
		}.bind(this), 1000);
		this.setState({batteryInterval: batteryInterval});
	}

	componentWillUnmount(){
		clearInterval(this.state.batteryInterval);
	}

	getDynamicMarkup(state){
		if(state === 'Discharging'){
			return(
				<div className={ 'floatable' }>
					<span id='battery-time' className={ 'floatLeft '}>
						<span>
							{ this.getChargeTime() }
						</span>
						<span className={ 'desc' }>until discharged</span>
					</span>
					<span id='battery' className={ 'floatRight' }>
						<span>{ Math.round( this.state.battery.level*100) }%</span>
						<FontIcon className={ 'material-icons powerIcon' }>battery_std</FontIcon>
					</span>
				</div>
			);
		}
		else if(state === 'Charging'){
			return(
				<div className={ 'floatable' }>
					<span id='battery-time' className={ 'floatLeft '}>
						<span>
							{ this.getChargeTime() }
						</span>
						<span className={ 'desc' }>until charged</span>
					</span>
					<span id='battery' className={ 'floatRight' }>
						<span>{ Math.round( this.state.battery.level*100) }%</span>
						<FontIcon className={ 'material-icons powerIcon' }>battery_charging_full</FontIcon>
					</span>
				</div>
			);
		}
		else{
			return(
				<div className={ 'floatable' }>
					<span id='battery-time' className={ 'floatLeft '}>
						<span>
							On power
						</span>
					</span>
					<span id='battery' className={ 'floatRight' }>
						<FontIcon className={ 'material-icons powerIcon' }>power</FontIcon>
					</span>
				</div>
			);
		}
	}

	returnPowerStateMarkup(){
		const charging        = this.state.battery.charging;
		const chargingTime    = this.state.battery.chargingTime;
		const dischargingTime = this.state.battery.dischargingTime;
		const level           = this.state.battery.level;

		if(
			(!charging) &&
			!isFinite(chargingTime) &&
			isFinite(dischargingTime) &&
			level < 1
		){
			return(this.getDynamicMarkup('Discharging'));
		}
		else if(
			(!charging) &&
			!isFinite(chargingTime) &&
			isFinite(dischargingTime) &&
			level === 1
		){
			return(this.getDynamicMarkup('On power'));
		}
		else if(
			(!charging) &&
			!isFinite(chargingTime) &&
			!isFinite(dischargingTime) &&
			level < 1
		){
			return(this.getDynamicMarkup('On power'));
		}
		else if(
			charging &&
			isFinite(chargingTime) &&
			!isFinite(dischargingTime) &&
			level < 1
		){
			return(this.getDynamicMarkup('Charging'));
		}
		else{
			return(this.getDynamicMarkup('On power'));
		}
	}

	render() {
		return (
			<Card className={ 'tile dashboardTile' }>
				<CardText>
					{ this.returnPowerStateMarkup() }
				</CardText>
			</Card>
		);
	}
}

export default PowerCard;
