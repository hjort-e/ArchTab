import React                          from 'react';
import { Card, CardText, CardHeader } from 'material-ui/Card';
import BookmarkGenerator              from './BookmarkGenerator.jsx';

class BookmarkCard extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			Bookmarks: null,
			BookmarksCount: 0
		};
	}

	getBookmarkItems(){
		chrome.bookmarks.getTree(
			function(bookmarks){
				window.bookmarks = bookmarks;
				this.setState({
					Bookmarks: bookmarks[0].children[ this.props.bookmarks ].children,
					BookmarksCount: bookmarks[0].children[ this.props.bookmarks ].children.length
				},() => {
					this.forceUpdate();
				});
			}.bind(this)
		);
	}

	componentWillMount(){
		this.getBookmarkItems();
	}

	render() {
		if(this.state.BookmarksCount === 0){
			return null;
		}
		else{
			return (
				<Card className={ 'tile dashboardTile' }>
					<CardHeader className={ 'cardTitle' } title={ this.props.title }/>
					<CardText>
						<BookmarkGenerator bookmarks={ this.state.Bookmarks } i={ this.state.BookmarksCount }/>
					</CardText>
				</Card>
			);
		}
	}
}

export default BookmarkCard;
