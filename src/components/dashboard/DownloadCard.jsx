import React                          from 'react';
import { Card, CardText, CardHeader } from 'material-ui/Card';
import FileGenerator                  from './FileGenerator.jsx';

class DownloadCard extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			Files: null,
			FilesCount: 0
		};
	}

	getDownloads(){
		chrome.downloads.search({ limit: 5, orderBy: ['-startTime'], exists: true, state: 'complete'},
			function(downloads){
				this.setState({
					Files: downloads,
					FilesCount: downloads.length
				},() => {
					this.forceUpdate();
				});
			}.bind(this)
		);
	}

	componentWillMount(){
		this.getDownloads();
	}

	componentDidMount(){
		this.getDownloads();
	}
	render() {
		if(this.state.FilesCount === 0){
			return null;
		}
		else{
			return (
				<Card className={ 'tile dashboardTile' }>
					<CardHeader className={ 'cardTitle' } title={ 'Downloads' }/>
					<CardText>
						<FileGenerator files={ this.state.Files } i={ this.state.FilesCount }/>
					</CardText>
				</Card>
			);
		}
	}
}

export default DownloadCard;
