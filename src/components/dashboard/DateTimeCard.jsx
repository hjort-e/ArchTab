import React              from 'react';
import { connect }        from 'react-redux';

import { Card, CardText } from 'material-ui/Card';

@connect((store) => {
	return {
		settings: store.settings
	};
})
class DateTimeCard extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			DateTime: { time: '00:00:00', meridiem: null, date: 'Thursday 1/0/1970'},
			timeInterval: null
		};
	}

	getDateTime(){
		const weekDays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		const dateTime = new Date();
		const year = dateTime.getFullYear();
		const month = dateTime.getMonth() + 1;
		const date = dateTime.getDate();
		const weekDay = weekDays[dateTime.getDay()];
		let hours = dateTime.getHours();
		let minutes = dateTime.getMinutes();
		let seconds = dateTime.getSeconds();

		let DateTime = {};
		if(this.props.settings.Use24HourFormat){
			if(hours < 10) {hours = '0' + hours;}
			if(minutes < 10) {minutes = '0' + minutes;}
			if(seconds < 10) {seconds = '0' + seconds;}

			DateTime.time = hours + ':' + minutes + (this.props.settings.ShowSeconds ? (':' + seconds) : '');
			DateTime.meridiem = null;
		}else{
			var meridiem = 'AM';

			if (hours > 12) {
				hours = hours - 12;
				meridiem = 'PM';
			}
			if (hours === 0) {hours = 12;}

			if(hours < 10) {hours = '0' + hours;}
			if(minutes < 10) {minutes = '0' + minutes;}
			if(seconds < 10) {seconds = '0' + seconds;}

			DateTime.time = hours + ':' + minutes + (this.props.settings.ShowSeconds ? (':' + seconds) : '');
			DateTime.meridiem = meridiem;
		}
		if(this.props.settings.UseAmericanDateFormat){
			DateTime.date = weekDay + ' - ' + month + '/' + date + '/' + year;
		}else{
			DateTime.date = weekDay + ' - ' + date + '/' + month + '/' + year;
		}

		return DateTime;
	}

	componentWillMount(){
		this.setState({
			DateTime: this.getDateTime()
		});
		let timeInterval = setInterval(function() {
			this.setState({
				DateTime: this.getDateTime()
			});
		}.bind(this), 100);
		this.setState({timeInterval: timeInterval});
	}

	componentWillUnmount(){
		clearInterval(this.state.timeInterval);
	}

	render() {
		return (
			<Card className={ 'tile dashboardTile' }>
				<CardText>
					<div id='clock' className={ this.props.settings.CenterClock ? 'center' : ''}>{ this.state.DateTime.time }<span>{ this.state.DateTime.meridiem }</span></div>
					<div className={ 'align-right' }>
						<div id='date'>{ this.state.DateTime.date }</div>
					</div>
				</CardText>
			</Card>
		);
	}
}

export default DateTimeCard;
