import React                                       from 'react';
import Link                                        from 'react-router/lib/Link';
import { connect }                                 from 'react-redux';
import Masonry                                     from 'react-masonry-component';
import FontIcon                                    from 'material-ui/FontIcon';
import FloatingActionButton                        from 'material-ui/FloatingActionButton';
import DocumentTitle                               from 'react-document-title';

import MuiThemeProvider                           from 'material-ui/styles/MuiThemeProvider';
import themes                                     from '../../styles/themes.js';

import DateTimeCard     from './DateTimeCard.jsx';
import ComputerInfoCard from './ComputerInfoCard.jsx';
import PowerCard        from './PowerCard.jsx';
import BookmarkCard     from './BookmarkCard.jsx';
import DownloadCard     from './DownloadCard.jsx';
import TopSitesCard     from './TopSitesCard.jsx';

const masonryOptions = {
	transitionDuration : '0.4s',
	itemSelector : '.tile',
	gutter: 20,
	fitWidth: true
};

@connect((store) => {
	return {
		settings: store.settings
	};
})
class Dashboard extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			DateTime: { time: '', meridiem: ''},
			timeInterval: null
		};

		if(Object.keys(this.props.settings).length === 0){
			this.props.dispatch({ type: 'RESET_SETTINGS'});
		}
		else{
			this.props.dispatch({ type: 'VERIFY_SETTINGS'});
		}
	}

	getDateTime(){
		const dateTime = new Date();
		let hours = dateTime.getHours();
		let minutes = dateTime.getMinutes();
		let seconds = dateTime.getSeconds();

		let DateTime = {};
		if(this.props.settings.Use24HourFormat){
			if(hours < 10) {hours = '0' + hours;}
			if(minutes < 10) {minutes = '0' + minutes;}
			if(seconds < 10) {seconds = '0' + seconds;}

			DateTime.time = hours + ':' + minutes + (this.props.settings.ShowSeconds ? (':' + seconds) : '');
			DateTime.meridiem = '';
		}else{
			var meridiem = 'AM';

			if (hours > 12) {
				hours = hours - 12;
				meridiem = 'PM';
			}
			if (hours === 0) {hours = 12;}

			if(hours < 10) {hours = '0' + hours;}
			if(minutes < 10) {minutes = '0' + minutes;}
			if(seconds < 10) {seconds = '0' + seconds;}

			DateTime.time = hours + ':' + minutes + (this.props.settings.ShowSeconds ? (':' + seconds) : '');
			DateTime.meridiem = meridiem;
		}

		return DateTime;
	}

	componentWillMount(){
		let timeInterval = setInterval(function() {
			this.setState({
				DateTime: this.getDateTime()
			});
		}.bind(this), 500);
		this.setState({timeInterval: timeInterval});
	}

	componentWillUnmount(){
		clearInterval(this.state.timeInterval);
	}

	render() {
		if(Object.keys(this.props.settings).length !== 0){
			return (
				<MuiThemeProvider muiTheme={ this.props.settings.UseDarkTheme ? themes.dark : themes.light }>
				<DocumentTitle  title={ 'ArchTab ' + this.state.DateTime.time + ' ' + this.state.DateTime.meridiem }>
				<div className={ this.props.settings.UseDarkTheme ? 'darkBG' : 'lightBG' }>
				<div className={ 'wrapper' }>
					<Masonry className={ 'masonryComp' } options={masonryOptions}>
						<div>
							<DateTimeCard/>
							{ this.props.settings.ShowComputerInfo ? <ComputerInfoCard/> : '' }
							{ this.props.settings.ShowPowerInfo ? <PowerCard/> : '' }
							{ this.props.settings.ShowBookmarkBar ? <BookmarkCard bookmarks={ 0 } title={ 'Bookmark bar' }/> : '' }
							{ this.props.settings.ShowOtherBookmarks ? <BookmarkCard bookmarks={ 1 } title={ 'Other bookmarks' }/> : '' }
							{ this.props.settings.ShowMobileBookmarks ? <BookmarkCard bookmarks={ 2 } title={ 'Mobile bookmarks' }/> : '' }
							{ this.props.settings.ShowTopSites ? <TopSitesCard/> : '' }
							{ this.props.settings.ShowDownloads ? <DownloadCard/> : '' }
							<Link to='/settings'>
								<FloatingActionButton className={ 'settingsButton' }>
									<FontIcon className={ 'material-icons' }>settings</FontIcon>
								</FloatingActionButton>
							</Link>
						</div>
					</Masonry>
				</div>
				</div>
				</DocumentTitle>
				</MuiThemeProvider>
			);
		}else{
			return null;
		}
	}
}

export default Dashboard;
