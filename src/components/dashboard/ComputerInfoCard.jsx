import React              from 'react';
import { Card, CardText } from 'material-ui/Card';
import LinearProgress     from 'material-ui/LinearProgress';
import ProgressBarGenerator from './ProgressBarGenerator.jsx';

class ComputerInfoCard extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			referenceCPUData: { numOfProcessors: 0 },
			oldCPUData: false,
			newCPUData: false,
			finalUsage: [],
			processorComps: null,
			mem: 0
		};
	}

	updateMem(){
		chrome.system.memory.getInfo(
			function(memResults){
				this.setState({
					mem: 100 - Math.floor(memResults.availableCapacity/memResults.capacity*100)
				});
			}.bind(this)
		);
	}

	getProcessors(){
		chrome.system.cpu.getInfo(
			function(CPUData){
				this.setState({
					referenceCPUData: CPUData
				});
			}.bind(this)
		);
	}

	updateCPU(){
		chrome.system.cpu.getInfo(
			function(newCPUData){
				this.setState({
					oldCPUData: this.state.newCPUData,
					newCPUData: newCPUData.processors
				});
			}.bind(this)
		);
	}

	calcCPUUsage(){
		let finalUsage = [];
		for(let i = 0; i < this.state.referenceCPUData.numOfProcessors; i++){
			if(this.state.newCPUData){
				let usage = this.state.newCPUData[i].usage;
				if (this.state.oldCPUData) {
					let oldUsage = this.state.oldCPUData[i].usage;

					finalUsage[i] = Math.floor((usage.kernel + usage.user - oldUsage.kernel - oldUsage.user) / (usage.total - oldUsage.total) * 100);
				} else {
					finalUsage[i] = Math.floor((usage.kernel + usage.user) / usage.total * 100);
				}
				if(finalUsage[i] < 10){
					finalUsage[i] = '0' + finalUsage[i];
				}
			}else{
				return false;
			}
		}
		this.setState({
			finalUsage: finalUsage
		});
	}

	componentWillMount(){
		this.getProcessors();
		this.updateCPU();
		this.calcCPUUsage();
		this.updateMem();
		let pcInfoInterval = setInterval(function() {
			this.updateCPU();
			this.calcCPUUsage();
			this.updateMem();
		}.bind(this), 1000);
		this.setState({ pcInfoInterval:  pcInfoInterval});
	}

	componentWillUnmount(){
		clearInterval(this.state.pcInfoInterval);
	}

	render(){
		return (
			<Card className={ 'tile dashboardTile' }>
				<CardText>
					<h4>CPU</h4>
					<ProgressBarGenerator cores={ this.state.referenceCPUData.numOfProcessors } finalUsage={ this.state.finalUsage }/>
					<h4>RAM</h4>
					<LinearProgress className={ 'progress progressBlue' } mode='determinate' value={ this.state.mem } />
				</CardText>
			</Card>
		);
	}
}

export default ComputerInfoCard;
