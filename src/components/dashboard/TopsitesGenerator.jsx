import React        from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import LinkHelper   from './LinkHelper.jsx';

class TopsitesGenerator extends React.Component {

	getSites(){
		let Sites = [];
		if(this.props.sites){
			for(let i = 0; i < this.props.i ; i++){
				if(LinkHelper.verifyUrl(this.props.sites[i].url)){
					Sites.push(
						<a href={ this.props.sites[i].url } key={ 'site' + i }>
							<RaisedButton
								className={ 'linkButton'}
								icon={ <img src={ 'chrome://favicon/' + this.props.sites[i].url }/> }
								label={ LinkHelper.createLinkTitle(this.props.sites[i], 23) }
							/>
						</a>
					);
				}
			}
			return Sites;
		}else{
			return 'Loading...';
		}
	}

	render(){
		return (
			<div>
				{  this.getSites() }
			</div>
		);
	}
}

export default TopsitesGenerator;
