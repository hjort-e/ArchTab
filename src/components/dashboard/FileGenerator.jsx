import React        from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon     from 'material-ui/FontIcon';
import LinkHelper   from './LinkHelper.jsx';

class FileGenerator extends React.Component {

	showFile(fileId){
		chrome.downloads.show(fileId);
	}

	getFiles(){
		let Files = [];
		if(this.props.files){
			for(let i = 0; i < this.props.i ; i++){
				Files.push(
					<RaisedButton
						key={ 'site' + i }
						className={ 'fileButton'}
						icon={ <FontIcon className={ 'material-icons' }>description</FontIcon> }
						label={ LinkHelper.createFileTitle(this.props.files[i].filename, 20) }
						onClick={ this.showFile.bind(this, (this.props.files[i].id)) }
					/>
				);
			}
			return Files;
		}else{
			return '';
		}
	}

	render(){
		return (
			<div>
				{  this.getFiles() }
			</div>
		);
	}
}

export default FileGenerator;
