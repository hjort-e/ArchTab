import React              from 'react';
import LinearProgress     from 'material-ui/LinearProgress';

class ProgressBarGenerator extends React.Component {

	getProcessors(){
		let Processors = [];
		for(let i = 0; i < this.props.cores; i++){
			Processors.push(<LinearProgress key={'core' + i} className={ 'progress progressGreen' } mode="determinate" value={ this.props.finalUsage[i] } />);
		}
		return Processors;
	}
	render(){
		return (
			<div>
				{  this.getProcessors() }
			</div>
		);
	}
}

export default ProgressBarGenerator;
