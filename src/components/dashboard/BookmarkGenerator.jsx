import React          from 'react';
import { connect }    from 'react-redux';
import RaisedButton   from 'material-ui/RaisedButton';
import FolderDialog   from './FolderDialog.jsx';
import FolderDropdown from './FolderDropdown.jsx';
import LinkHelper     from './LinkHelper.jsx';

@connect((store) => {
	return {
		settings: store.settings
	};
})
class BookmarkGenerator extends React.Component {

	getBookmarks(){
		let Bookmarks = [];
		if(this.props.bookmarks){
			for(let i = 0; i < this.props.i ; i++){

				// Normal bookmarks
				if(LinkHelper.determineType(this.props.bookmarks[i]) === 'Link'){
					Bookmarks.push(
						<a href={ this.props.bookmarks[i].url } key={ 'bkmk' + i }>
							<RaisedButton
								className={ 'linkButton'}
								icon={ <img src={ 'chrome://favicon/' + this.props.bookmarks[i].url }/> }
								label={ LinkHelper.createLinkTitle(this.props.bookmarks[i], 23) }
							/>
						</a>
					);
				}
				else if(LinkHelper.determineType(this.props.bookmarks[i]) === 'Folder'){
					if (this.props.settings.UseAltBookmarkFolders) {
						Bookmarks.push(
							<FolderDropdown
								title={ LinkHelper.trimTitle(this.props.bookmarks[i].title, 20) }
								key={ 'bkmk' + i }
								children={ this.props.bookmarks[i].children }
							/>
						);
					}else{
						Bookmarks.push(
							<FolderDialog
								title={ LinkHelper.trimTitle(this.props.bookmarks[i].title, 20) }
								key={ 'bkmk' + i }
								children={ this.props.bookmarks[i].children }
							/>
						);
					}
				}

			}
			return Bookmarks;
		}else{
			return 'Loading...';
		}
	}

	render(){
		return (
			<div>
				{  this.getBookmarks() }
			</div>
		);
	}
}

export default BookmarkGenerator;
