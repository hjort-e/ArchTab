import React            from 'react';
import Dialog           from 'material-ui/Dialog';
import FlatButton       from 'material-ui/FlatButton';
import RaisedButton     from 'material-ui/RaisedButton';
import FontIcon         from 'material-ui/FontIcon';
import {List, ListItem} from 'material-ui/List';
import Popover          from 'material-ui/Popover';
import Menu             from 'material-ui/Menu';
import MenuItem         from 'material-ui/MenuItem';
import LinkHelper       from './LinkHelper.jsx';

class FolderDropdown extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			open: false,
			dialogOpen: false,
			parentBookmarks: [null],
			parentDialogTitle: [null],
		};
	}

	handleDialogOpen = (child) => {
		this.setState({
			open: false,
			dialogOpen: true,
			bookmarks: child.children,
			dialogTitle: child.title
		});
	}

	handleDialogClose = () => {
		this.setState({
			dialogOpen: false,
			bookmarks: this.props.children,
			dialogTitle: '',
			parentBookmarks: [null],
			parentDialogTitle: [null],
		});
	}

	handleSubFolder = (child) => {
		this.setState({
			parentBookmarks: this.state.parentBookmarks.concat([this.state.bookmarks]),
			parentDialogTitle: this.state.parentDialogTitle.concat([this.state.dialogTitle]),
			bookmarks: child.children,
			dialogTitle: child.title
		});
	}

	handleParentFolder = () => {
		this.setState({
			bookmarks: this.state.parentBookmarks[this.state.parentBookmarks.length - 1],
			dialogTitle: this.state.parentDialogTitle[this.state.parentDialogTitle.length - 1],
			parentBookmarks: this.state.parentBookmarks.slice(0, -1),
			parentDialogTitle: this.state.parentDialogTitle.slice(0, -1),
		});
	}

	handleOpen = (event) => {
		event.preventDefault();
		this.setState({
			open: true,
			anchorEl: event.currentTarget,
		});
	}

	handleClose = () => {
		this.setState({
			open: false,
		});
	}

	getOtherBookmarks(){
		let Bookmarks = [];
		if(this.state.parentBookmarks[this.state.parentBookmarks.length - 1] !== null){
			Bookmarks.push(
				<ListItem
				leftIcon={ <FontIcon className={ 'material-icons' }>keyboard_backspace</FontIcon>}
				primaryText={ 'Back' }
				key={'plzstop'}
				onClick={ this.handleParentFolder.bind(this) }
				/>
			);
		}
		if(this.state.bookmarks && this.state.bookmarks.length > 0){
			for(let i = 0; i < this.state.bookmarks.length ; i++){

				// Normal bookmarks
				if(LinkHelper.determineType(this.state.bookmarks[i]) === 'Link'){
					Bookmarks.push(
						<a href={ this.state.bookmarks[i].url } key={ 'bkmk' + i }>
							<ListItem
								leftIcon={ <img src={ 'chrome://favicon/' + this.state.bookmarks[i].url }/> }
								primaryText={ LinkHelper.createLinkTitle(this.state.bookmarks[i], 70) }
							/>
						</a>
					);
				}
				else if(LinkHelper.determineType(this.state.bookmarks[i]) === 'Folder'){
					Bookmarks.push(
						<ListItem
							leftIcon={ <FontIcon className={ 'material-icons' }>folder</FontIcon>}
							primaryText={ LinkHelper.trimTitle(this.state.bookmarks[i].title, 70) }
							key={ 'bkmk' + i }
							onClick={ this.handleSubFolder.bind(this, this.state.bookmarks[i] ) }
						/>
					);
				}
			}
		}else{
			Bookmarks.push(
				<ListItem
				primaryText={ 'This folder is empty' }
				key={ 'plzstopity' }
				disabled={true}
				className={ 'emptyMsg' }
				/>
			);
		}

		return Bookmarks;
	}

	getBookmarks(){
		let Bookmarks = [];
		if(this.props.children && this.props.children.length > 0){
			for(let i = 0; i < this.props.children.length ; i++){

				// Normal bookmarks
				if(LinkHelper.determineType(this.props.children[i]) === 'Link'){
					Bookmarks.push(
						<a href={ this.props.children[i].url } key={ 'bkmk' + i }>
							<MenuItem
								leftIcon={ <img src={ 'chrome://favicon/' + this.props.children[i].url }/> }
								primaryText={ LinkHelper.createLinkTitle(this.props.children[i], 70) }
							/>
						</a>
					);
				}
				else if(LinkHelper.determineType(this.props.children[i]) === 'Folder'){
					Bookmarks.push(
						<span key={ 'bkmk' + i }>
							<MenuItem
								leftIcon={ <FontIcon className={ 'material-icons' }>folder</FontIcon>}
								primaryText={ LinkHelper.trimTitle(this.props.children[i].title, 70) }
								onClick={ this.handleDialogOpen.bind(this, this.props.children[i]) }
							/>
						</span>
					);
				}
			}
		}else{
			Bookmarks.push(
				<MenuItem
				primaryText={ 'This folder is empty' }
				key={ 'plzstopity' }
				disabled={true}
				className={ 'emptyMsg' }
				/>
			);
		}

		return Bookmarks;
	}

	render() {

		return (
			<span>
				<RaisedButton
					className={ 'linkButton'}
					label={ this.props.title }
					icon={ <FontIcon className={ 'material-icons' }>folder</FontIcon> }
					onClick={ this.handleOpen.bind(this) }
				/>
				<Popover
					open={this.state.open}
					anchorEl={this.state.anchorEl}
					anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
					targetOrigin={{horizontal: 'left', vertical: 'top'}}
					onRequestClose={ this.handleClose.bind(this) }
				>
					<Menu>
						{ this.getBookmarks() }
					</Menu>
				</Popover>
				<Dialog
					title={ this.state.dialogTitle }
					actions={(<FlatButton
						label="Close"
						primary={true}
						onTouchTap={ this.handleDialogClose.bind(this) }
					/>)}
					modal={ false }
					open={ this.state.dialogOpen }
					onRequestClose={ this.handleDialogClose.bind(this) }
					autoScrollBodyContent={ true }
					className={ 'FolderDialogComponent' }
				>
					<List>
						{ this.getOtherBookmarks() }
					</List>
				</Dialog>
			</span>
		);
	}
}

export default FolderDropdown;
