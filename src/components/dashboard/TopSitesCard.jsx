import React                          from 'react';
import { Card, CardText, CardHeader } from 'material-ui/Card';
import TopSitesGenerator              from './TopsitesGenerator.jsx';

class TopSitesCard extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			TopSites: null,
			TopSitesCount: 0
		};
	}

	getTopSites(){
		chrome.topSites.get(
			function(sites){
				this.setState({
					TopSites: sites,
					TopSitesCount: sites.length
				},() => {
					this.forceUpdate();
				});
			}.bind(this)
		);
	}

	componentWillMount(){
		this.getTopSites();
	}

	render() {
		if(this.state.TopSitesCount === 0){
			return null;
		}
		else{
			return (
				<Card className={ 'tile dashboardTile' }>
					<CardHeader className={ 'cardTitle' } title={ 'Top sites' }/>
					<CardText>
						<TopSitesGenerator sites={ this.state.TopSites } i={ this.state.TopSitesCount }/>
					</CardText>
				</Card>
			);
		}
	}
}

export default TopSitesCard;
