import React            from 'react';
import Dialog           from 'material-ui/Dialog';
import FlatButton       from 'material-ui/FlatButton';
import RaisedButton     from 'material-ui/RaisedButton';
import FontIcon         from 'material-ui/FontIcon';
import {List, ListItem} from 'material-ui/List';
import LinkHelper       from './LinkHelper.jsx';

class FolderDialog extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			open: false,
			parentBookmarks: [null],
			parentTitle: [null],
			bookmarks: this.props.children,
			title: this.props.title
		};
	}

	handleSubFolder = (child) => {
		this.setState({
			parentBookmarks: this.state.parentBookmarks.concat([this.state.bookmarks]),
			parentTitle: this.state.parentTitle.concat([this.state.title]),
			bookmarks: child.children,
			title: child.title
		});
	}

	handleParentFolder = () => {
		this.setState({
			bookmarks: this.state.parentBookmarks[this.state.parentBookmarks.length - 1],
			title: this.state.parentTitle[this.state.parentTitle.length - 1],
			parentBookmarks: this.state.parentBookmarks.slice(0, -1),
			parentTitle: this.state.parentTitle.slice(0, -1),
		});
	}

	handleOpen = () => {
		this.setState({open: true});
	}

	handleClose = () => {
		this.setState({
			open: false,
			bookmarks: this.props.children,
			title: this.props.title,
			parentBookmarks: [null],
			parentTitle: [null],
		});
	}

	getBookmarks(){
		let Bookmarks = [];
		if(this.state.parentBookmarks[this.state.parentBookmarks.length - 1] !== null){
			Bookmarks.push(
				<ListItem
				leftIcon={ <FontIcon className={ 'material-icons' }>keyboard_backspace</FontIcon>}
				primaryText={ 'Back' }
				key={'plzstop'}
				onClick={ this.handleParentFolder.bind(this) }
				/>
			);
		}
		if(this.state.bookmarks && this.state.bookmarks.length > 0){
			for(let i = 0; i < this.state.bookmarks.length ; i++){

				// Normal bookmarks
				if(LinkHelper.determineType(this.state.bookmarks[i]) === 'Link'){
					Bookmarks.push(
						<a href={ this.state.bookmarks[i].url } key={ 'bkmk' + i }>
							<ListItem
								leftIcon={ <img src={ 'chrome://favicon/' + this.state.bookmarks[i].url }/> }
								primaryText={ LinkHelper.createLinkTitle(this.state.bookmarks[i], 70) }
							/>
						</a>
					);
				}
				else if(LinkHelper.determineType(this.state.bookmarks[i]) === 'Folder'){
					Bookmarks.push(
						<ListItem
							leftIcon={ <FontIcon className={ 'material-icons' }>folder</FontIcon>}
							primaryText={ LinkHelper.trimTitle(this.state.bookmarks[i].title, 70) }
							key={ 'bkmk' + i }
							onClick={ this.handleSubFolder.bind(this, this.state.bookmarks[i] ) }
						/>
					);
				}
			}
		}else{
			Bookmarks.push(
				<ListItem
				primaryText={ 'This folder is empty' }
				key={ 'plzstopity' }
				disabled={true}
				className={ 'emptyMsg' }
				/>
			);
		}

		return Bookmarks;
	}

	render() {
		const actions = [
			<FlatButton
				label="Close"
				primary={true}
				onTouchTap={ this.handleClose }
			/>
		];

		return (
			<span>
				<RaisedButton
					className={ 'linkButton'}
					label={ this.props.title }
					icon={ <FontIcon className={ 'material-icons' }>folder</FontIcon> }
					onClick={ this.handleOpen.bind(this) }
				/>
				<Dialog
					title={ this.state.title }
					actions={actions}
					modal={false}
					open={this.state.open}
					onRequestClose={this.handleClose}
					autoScrollBodyContent={true}
					className={ 'FolderDialogComponent' }
				>
					<List>
						{ this.getBookmarks() }
					</List>
				</Dialog>
			</span>
		);
	}
}

export default FolderDialog;
