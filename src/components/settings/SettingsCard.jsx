import React       from 'react';
import { connect } from 'react-redux';
import Paper       from 'material-ui/Paper';
import FlatButton  from 'material-ui/FlatButton';
import Toggle      from 'material-ui/Toggle';
import Divider     from 'material-ui/Divider';

@connect((store) => {
	return {
		settings: store.settings
	};
})
class SettingsCard extends React.Component {

	toggleSetting( setting ){
		this.props.dispatch({ type: 'TOGGLE_SETTING', payload: setting});
	}

	resetSettings(){
		this.props.dispatch({ type: 'RESET_SETTINGS'});
	}

	render() {
		return (
			<Paper className={ 'tile settingsTile settingsCard' }>
				<h2>Settings</h2>
				<p>Below you can toggle which cards you want to be shown on your startpage.</p>
				<h3>Device info</h3>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.ShowComputerInfo } onClick={ this.toggleSetting.bind( this, 'ShowComputerInfo' ) } label="Show CPU and RAM load of your device" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.ShowPowerInfo } onClick={ this.toggleSetting.bind( this, 'ShowPowerInfo' ) } label="Show power status of your device" />
				</div>
				<Divider />
				<h3>Bookmarks</h3>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.ShowBookmarkBar } onClick={ this.toggleSetting.bind( this, 'ShowBookmarkBar' ) } label="Show bookmarksbar" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.ShowOtherBookmarks } onClick={ this.toggleSetting.bind( this, 'ShowOtherBookmarks' ) } label="Show other bookmarks" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.ShowMobileBookmarks } onClick={ this.toggleSetting.bind( this, 'ShowMobileBookmarks' ) } label="Show mobile bookmarks" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.UseAltBookmarkFolders } onClick={ this.toggleSetting.bind( this, 'UseAltBookmarkFolders' ) } label="Use alternative bookmark folders" />
				</div>
				<Divider />
				<h3>Time and date</h3>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.Use24HourFormat } onClick={ this.toggleSetting.bind( this, 'Use24HourFormat' ) } label="Use 24 hour clock" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.ShowSeconds } onClick={ this.toggleSetting.bind( this, 'ShowSeconds' ) } label="Show seconds on clock" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.CenterClock } onClick={ this.toggleSetting.bind( this, 'CenterClock' ) } label="Horizontally center clock" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.UseAmericanDateFormat } onClick={ this.toggleSetting.bind( this, 'UseAmericanDateFormat' ) } label="Use American date format" />
				</div>
				<Divider />
				<h3>Miscellaneous</h3>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.ShowTopSites } onClick={ this.toggleSetting.bind( this, 'ShowTopSites' ) } label="Show top sites" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.ShowDownloads } onClick={ this.toggleSetting.bind( this, 'ShowDownloads' ) } label="Show recent downloads" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<Toggle toggled={ this.props.settings.UseDarkTheme } onClick={ this.toggleSetting.bind( this, 'UseDarkTheme' ) } label="Use dark theme" />
				</div>
				<Divider />
				<div className={ 'settingsRow' }>
					<FlatButton secondary={true} onClick={ this.resetSettings.bind(this) } label="Reset settings"/>
				</div>
			</Paper>
		);
	}
}

export default SettingsCard;
