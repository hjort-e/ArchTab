import React                          from 'react';
import { Card, CardText, CardHeader } from 'material-ui/Card';
import FlatButton                     from 'material-ui/FlatButton';

class AboutCard extends React.Component {
	render() {
		return (
			<Card className={ 'tile settingsTile' }>
				<CardHeader className={ 'cardTitle bigHeader' } title="About"/>
				<CardText>
					Thank you for using ArchTab v1.1!<br/>
					This version was built with React v15.3, Redux v3.6 and Material-UI v0.16.1<br/>
					It's open source and you can check it out on <FlatButton secondary={true} href="https://gitlab.com/hjort-e/ArchTab">Gitlab</FlatButton>
				</CardText>
			</Card>
		);
	}
}

export default AboutCard;
