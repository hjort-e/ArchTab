import React                          from 'react';
import { Card, CardText, CardHeader } from 'material-ui/Card';

class ChangelogCard extends React.Component {
	render() {
		return (
			<Card className={ 'tile settingsTile' }>
				<CardHeader className={ 'cardTitle bigHeader' } title="Changelog"/>
				<CardText>
					Heres whats new in v1.1!
					<ul>
						<li>Bookmark folders are now supported</li>
						<li>Performance improvements</li>
					</ul>
					<h4>Planned features</h4>
					<ul>
						<li>Blacklisting of URL's from bookmarks lists</li>
						<li>More themes</li>
						<li>Sync of settings across devices</li>
						<li>Fancier downloads card</li>
						<li>Weather card</li>
						<li>NASA picture of the day</li>
						<li>Quick notes</li>
					</ul>
				</CardText>
			</Card>
		);
	}
}

export default ChangelogCard;
