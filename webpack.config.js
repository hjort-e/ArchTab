var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {

};
var config = {
	entry: APP_DIR + '/index.jsx',
	output: {
		path: BUILD_DIR,
		filename: 'bundle.js'
	},
	module : {
		preLoaders: [
			{
				test: /\.jsx?$/,
				loaders: ['eslint'],
				include: APP_DIR
			}
		],
		loaders : [
			{
				test : /\.jsx?/,
				include : APP_DIR,
				loader : 'babel'
			},
			{
				test: /\.sass$/,
				loaders: ["style", "css", "sass"]
			},
			{
				test: /\.(ttf)$/,
				loader: 'file'
			}
		]
	}
};

module.exports = config;
